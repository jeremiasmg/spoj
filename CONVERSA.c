#include<stdio.h>
int main(){
	char car,end;
	//Eliminates the first 'p'
	getchar_unlocked();
	while((car = getchar_unlocked())!=EOF){
		end = getchar_unlocked();
		//Faster function for char
		putchar_unlocked((car!=32) ? ( (car>109) ? (car-13) : (car>96) ? (car+13) : (car>77) ? (car-13) : (car+13)  ) : car);
		if(end==10){
			printf("\n");
			//Eliminates the first 'p'
			getchar_unlocked();
		}
	}
	return(0);
}
