#include<stdio.h>
#include<string.h>
#define MOD 1300031
long long int v[100001];

int getInt(void){
	int s=0;
	char c=0;
	for(;c<'0'; c=getchar_unlocked());
	for(;c>='0';c=getchar_unlocked()){
		s = 10*s + c-'0';
	}
	return(s);
}

long long int getLLInt(void){
	long long int s=0;
	char c=0;
	for(;c<'0'; c=getchar_unlocked());
	for(;c>='0';c=getchar_unlocked()){
		s = 10*s + c-'0';
	}
	return(s);
}

int main(){
	int t,n,i,j;
	long long int soma, total;
	t = getInt();
	while(t--){
		total = 0;
		n = getInt();
		for(i=0;i<n;i=i+1){
			v[i] = getLLInt();
			total = total + v[i];
		}
		soma = 0;
		for(i=0;i<n;i=i+1){
			soma = soma + (v[i]%MOD*total%MOD);
			total = total - v[i];
		}
		printf("%lld\n",soma % MOD);
	}
	return(0);
}
